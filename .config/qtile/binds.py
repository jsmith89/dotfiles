from libqtile.config import Key
from libqtile.command import lazy
from libqtile.lazy import lazy

mod = "mod4"
myTerm = "kitty"
myBrowser = "brave"

#Binds - Shortcuts
keys = [
    Key([mod, "shift"], "c",
        lazy.spawn("caprine"),
        ),
    Key([mod], "Return",
        lazy.spawn('kitty'),
        ),
    Key([mod, "shift"], "Return",
        lazy.spawn("rofi -show drun"),
        ),
    Key([mod], "w",
        lazy.spawn("brave"),
        ),
    Key([mod], "d",
        lazy.spawn("neovim"),
        ),
    Key([mod], "f1",
        lazy.spawn("thunar"),
        ),
    Key([mod], "r",
        lazy.spawn("kitty -e ranger"),
        ),
    Key([mod], "e",
        lazy.spawn("nvim"),
        ),
    Key([mod, "shift"], "e",
        lazy.spawn("code"),
        ),
    Key([mod], "Tab",
        lazy.next_layout(),
        desc='Toggle through layouts'
        ),
    Key([mod], "q",
        lazy.window.kill(),
        desc='Kill active window'
        ),
    Key([mod, "shift"], "r",
        lazy.restart(),
        desc='Restart Qtile'
        ),
    Key([mod, "shift"], "x",
        lazy.shutdown(),
        desc='Shutdown Qtile'
        ),
    Key([mod], "l",
        lazy.next_screen(),
        desc='Move focus to next monitor'
        ),
    Key([mod], "h",
        lazy.prev_screen(),
        desc='Move focus to prev monitor'
        ),
    Key([mod], "j",
        lazy.layout.down(),
        desc='Move focus down in current stack pane'
        ),
    Key([mod], "k",
        lazy.layout.up(),
        desc='Move focus up in current stack pane'
        ),
    Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        lazy.layout.section_right(),
        desc='Move windows down in current stack'
        ),
    Key([mod, "shift"], "h",
        lazy.layout.shuffle_left(),
        lazy.layout.section_left(),
        desc='Move windows up in current stack'
        ),
    Key([mod, "control"], "l",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod, "control"], "h",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod, "control"], "j",
        lazy.layout.shrink(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
        ),
    Key([mod, "control"], "k",
        lazy.layout.grow(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'
        ),
    Key([mod], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'
        ),
    Key([mod], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'
        ),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'
        ),
    Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'
        ),
    Key([mod, "shift"], "Tab",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'
        ),
    Key([mod], "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'
        ),
    Key([mod, "shift"], "space",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'
        ),
]