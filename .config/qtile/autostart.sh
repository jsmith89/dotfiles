#!/bin/bash
nm-applet &
blueman-manager &
nitrogen --restore &
flameshot &
discord &
razercommander &
#feh --bg-scale ~/Documents/wallpapers/wallpaperflare.com_wallpaper.jpg --output DP-2 &
#feh --bg-scale ~/Documents/wallpapers/IMG_0094.JPG --output HDMI-0 &
#xrandr --output HDMI-0 --mode 2560x1440 --rate 144 --pos 0x0 --primary \
#       --output DP-2 --mode 2560x1440 --rate 165 --pos 2560x0 
