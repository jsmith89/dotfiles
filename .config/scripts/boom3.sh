#!/bin/bash

# Restart Bluetooth service
echo "Restarting Bluetooth service..."
sudo systemctl restart bluetooth.service

# Wait for the service to restart
sleep 2

# Wait for the services to restart
sleep 2

# Connect to the Bluetooth device
echo "Connecting to Bluetooth device..."
echo -e 'connect 10:94:97:0A:C5:27\nexit' | bluetoothctl

sleep 2
echo "Operations completed."
