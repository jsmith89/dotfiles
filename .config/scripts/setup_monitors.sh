#!/bin/bash

xrandr --output HDMI-0 --mode 2560x1440 --rate 144 --pos 0x0 --primary \
       --output DP-2 --mode 2560x1440 --rate 144 --pos 2560x0
