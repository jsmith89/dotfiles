#
# ~/.bashrc for bashconf

#disable mouse accel
#xinput set-prop 8 'libinput Accel Profile Enabled' 0, 1
#xinput set-prop 9 'libinput Accel Profile Enabled' 0, 1

# jsmith aliases
alias boom3='./boom3.sh'
alias reboot='sudo reboot'
alias fonts='kitty list-fonts'
alias dl='cd ~/Downloads'
alias configs='cd ~/.config'
alias dqtile='cd ~/.config/qtile'
alias dnvim='cd ~/.config/nvim'
alias projects='cd ~/Projects'
alias quake='cd ~/nquake && ./quake.AppImage'

### SETTING OTHER ENVIRONMENT VARIABLES
if [ -z "$XDG_CONFIG_HOME" ]; then
	export XDG_CONFIG_HOME="$HOME/.config"
fi
if [ -z "$XDG_DATA_HOME" ]; then
	export XDG_DATA_HOME="$HOME/.local/share"
fi
if [ -z "$XDG_CACHE_HOME" ]; then
	export XDG_CACHE_HOME="$HOME/.cache"
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# extract
ex() {
	if [ -f "$1" ]; then
		case $1 in
		*.tar.bz2) tar xjf $1 ;;
		*.tar.gz) tar xzf $1 ;;
		*.bz2) bunzip2 $1 ;;
		*.rar) unrar x $1 ;;
		*.gz) gunzip $1 ;;
		*.tar) tar xf $1 ;;
		*.tbz2) tar xjf $1 ;;
		*.tgz) tar xzf $1 ;;
		*.zip) unzip $1 ;;
		*.Z) uncompress $1 ;;
		*.7z) 7z x $1 ;;
		*.deb) ar x $1 ;;
		*.tar.xz) tar xf $1 ;;
		*.tar.zst) unzstd $1 ;;
		*) echo "'$1' cannot be extracted via ex()" ;;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

# jsmith
alias tweaks="sudo archlinux-tweak-tool"
alias edit="nvim"
alias cd..="cd .."
alias cd.="cd .."
alias pacman='sudo pacman'

# history
HISTSIZE=1000
HISTFILESIZE=2000
HISTCONTROL=ignoreboth

# shopt
shopt -s nocaseglob
shopt -s checkwinsize
shopt -s autocd  # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend     # do not overwrite history
shopt -s expand_aliases # expand aliases
shopt -s checkwinsize   # checks term size when bash regains control

# Changing "ls" to "exa"
alias ls='exa -al --icons --color=always --group-directories-first' # my preferred listing
alias la='exa -a --icons --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --icons --color=always --group-directories-first'  # long format
alias l.='exa -al --icons --color=always --group-directories-first | egrep "^\."'

bind "set completion-ignore-case on"

eval "$(starship init bash)"

[ -f "/home/jsmith/.ghcup/env" ] && . "/home/jsmith/.ghcup/env" # ghcup-env

